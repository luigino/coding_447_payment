module gitee.com/luigino/coding_447_payment

go 1.16

require (
	gitee.com/luigino/coding_447_common v0.0.1
	github.com/golang/protobuf v1.4.3
	github.com/jinzhu/gorm v1.9.16
	github.com/micro/go-micro/v2 v2.9.1
	github.com/micro/go-plugins/registry/consul/v2 v2.9.1
	github.com/micro/go-plugins/wrapper/monitoring/prometheus/v2 v2.9.1 // indirect
	github.com/micro/go-plugins/wrapper/ratelimiter/uber/v2 v2.9.1 // indirect
	github.com/micro/go-plugins/wrapper/trace/opentracing/v2 v2.9.1 // indirect
)
